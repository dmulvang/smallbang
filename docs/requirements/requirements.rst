Requirements
============

Requirements for what the new documentation framework for FLACS **must** support:
---------------------------------------------------------------------------------

* Web based
   * Sphinx/ReStructuredText handles this by generating html
   * Served/hosted by ReadTheDocs
* Good handling of images and video
   * Sphinx/ReStructuredText handles this by embedding html code to include local or remote (You Tube) videos
* Easy to write mathematical formulas
   * Sphinx/ReStructuredText support math formulas in LaTex markup format
* Branding - FLACS-CFD, etc.
   * Sphinx/ReStructuredText handles this throug the laypot templates that might be tweaked
* Functional search feature
   * Sphinx/ReStructuredText handles this 
* Spell checking
   * Has do be done outside of the framework using editors with spell checking
   * Maybe an external spell checker can be used to run as an automatic test during Merge Request builds?


Other factors to consider:
--------------------------

* Access management
   * ReadTheDocs support access 
* Document versioning (labeling official version of the documentation)
   * Handled by ReadTheDocs based on branches/tags in Gitlab
* Version Control System (VCS) for handling source code
   * GitLab, but it has to be external to give access from ReadTheDocs
* Workflow support (draft, review, verification, proof-reading, etc)
   * Keep the current workflow based on Jira and GitLab, but trigger MR builds in ReadTheDocs
* Backup
   * Handled by GitLab.com ?
