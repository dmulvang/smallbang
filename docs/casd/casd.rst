CASD
====

CASD is an acronym for Computer Aided Scenario Design. The preprocessor CASD for the CFD simulator
FLACS-CFD is used to prepare the input data, or job data, that defines a FLACS-CFD simulation. This
comprises the: geometry model, computational grid, porosities, and scenario description.


What to do
----------
You can do this!

.. _my-reference-label:

What not to do
--------------

You should not do this!



What absolutley not to do
~~~~~~~~~~~~~~~~~~~~~~~~~
Never do this!
It refers to the section itself, see :ref:`variables-I-like-ref`.

Or if you like figures: :ref:`fig-ref-01`

Or if you like figures: fig-ref-01_

Or ref figure by numbers: :numref:`fig-ref-02`


Or ref table by caption: :ref:`table-ref2`

Or ref table by numbers: :numref:`table-ref2`

Things to consider
------------------

Maybe you should do this

.. _fig-ref-01:
.. figure:: /_static/picture.jpg
   :alt: map to buried treasure

   This is the caption of the figure (a simple paragraph).

.. _fig-ref-02:
.. figure:: /_static/picture.jpg
   :scale: 50 %
   :alt: map to buried treasure

   This is the caption of the figure (a simple paragraph).

   The legend consists of all elements after the caption.  In this
   case, the legend consists of this paragraph and the following 
   table:
   
   .. |tent| image:: /_static/tent.png
   .. |wave| image:: /_static/waves.png
   .. |peak| image:: /_static/peak.png

   +--------+------------+
   | Symbol | Meaning    |
   +========+============+
   | |tent| | Campground |
   +--------+------------+
   | |wave| | Lake       |
   +--------+------------+
   | |peak| | Mountain   |
   +--------+------------+


.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/hlPjKc4BaHo?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

.. _table-ref2:
.. table:: Enlightment

   +----+------+
   |Name|Number|
   +====+======+
   |A   |7     |
   +----+------+
   |B   |9     |
   +----+------+
   |U   |2     |
   +----+------+

HEPP
====

.. raw:: html

    <video width="320" height="240" controls source src="../_static/video-01.mp4" type="video/mp4">
    Your browser does not support the video tag.
    </video>
