Flowvis
=======

Flowvis, the postprocessor for the CFD-code FLACS, is a program for visualising results from simulations of gas explosions, gas dispersion and multiphase flow. Flowvis is the latest generation of the visualisation tool and included in the FLACS package.

NB! Some things you should never do, see :ref:`my-reference-label`.

.. _variables-I-like-ref:

Variables I *like* to visualize
-------------------------------
* A
* B
* C


Variables I **hate** to visualize
---------------------------------
1. Alpha
2. Beta
3. Charlie
4. Delta

   * Hepp
   * HIPP