Potential issues with using ReadTheDocs
=======================================

* GitLab has to be on a public server
   * Exposes source code if we want documentation build from source, e.g. Python API
   * Increased distance between source code and documentation code
