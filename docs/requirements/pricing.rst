Pricing
=======

ReadTheDocs
-----------

`ReadTheDocs pricing <https://readthedocs.com/pricing/>`_

$50, $150 or $250 /month


GitLab
------

`GitLab pricing <https://about.gitlab.com/pricing/>`_

$19 or $99 /user/month