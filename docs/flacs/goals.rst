Goals for improved FLACS documentation
======================================

Spesific documentation requirements:
------------------------------------

* Move from chapters named "3.3 Geometry menu", to "Importing geometry", "Setting up the grid", etc.
* Start of at "FLACS for Dummies", so that the user can drill down to more technical supporting documentation when needed
* Make tutorials: "To perform an explosion simulation you do X, Y and Z."
* Give clear and strong guidelines that match what we do in validation/testing

Other supporting actions
------------------------

* Build guidelines into the application, using meaningful tooltips, warnings and error messages
* Limit the users initial choices, and move functionality behind an "advanced option" barrier
* Make the software so user friendly that the user manual is not needed.