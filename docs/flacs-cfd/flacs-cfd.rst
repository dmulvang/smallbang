FLACS-CFD
=========

Our powerful industry-leading computational fluid dynamics (CFD) software simulates the dispersion of hazardous materials, fire, and explosion with results that you can trust.
Designed for ease of use and multiple environments, FLACS-CFD represents more than 40 years of extensive and unrivalled modelling and validation work based on real-life testing.
FLACS-CFD is a suite of 3D computational fluid dynamic tools with a series of standard modules and additional bolt-ons designed to meet specific requirements. These can be leased separately or as a package and are effective for a wide range of users including facility operators who require quick answers to engineering questions through to consultants who need to perform large in-depth risk studies. All licence holders receive full training on how to use the software.

With continuous development and investment, FLACS-CFD helps raise the true level of safety for complex industrial environments.


Math examples:

.. math::
  α_t(i) = P(O_1, O_2, … O_t, q_t = S_i λ)

.. math::
  c = 1 - \frac{Y_F}{F_{F0}}

.. math::
  \qquad R_F = C_{\beta R_F}\frac{S}{\Delta}\rho \min \left[ c,9(1-c) \right]

.. math::
  \frac{\partial}{\partial t} \left( \beta_v \rho Y_{fuel}\right)
  \frac{\partial}{\partial x_j} \left(\beta_j\rho u_j Y_{fuel}\right) =
  \frac{\partial}{\partial x_j} \left(\beta_j\rho D \frac{\partial Y_{fuel}}{\partial x_j} \right) + R_F

  
.. warning::
  Not everyone likes math! See :ref:`variables-I-like-ref`.